# --- Base16 Shell ---
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        source "$BASE16_SHELL/profile_helper.sh"

base16_material-darker


# --- env variables ---
export VISUAL=nvim
export EDITOR="$VISUAL"

export ANDROID_HOME=$HOME/Android/Sdk
export ANDROID_SDK_ROOT=$HOME/Android/Sdk
export ANDROID_AVD_HOME=$HOME/.android/avd

export FLUTTER_ROOT=$HOME/flutter

export CHROME_EXECUTABLE=google-chrome-stable

# Source local.env variables
[[ -f "${HOME}/.local.env" ]] && source "${HOME}/.local.env"

export PATH=$PATH:$HOME/bin/:$HOME/go/bin/:$HOME/flutter/bin/:$HOME/.pub-cache/bin


# --- zsh plugins ---
source "${HOME}/.zgen/zgen.zsh"

# if the init script doesn't exist
if ! zgen saved; then
    # plugins
    zgen oh-my-zsh
    zgen oh-my-zsh themes/ys
    zgen oh-my-zsh plugins/git
    zgen oh-my-zsh plugins/ssh-agent
    zgen oh-my-zsh plugins/kubectl
    zgen load zsh-users/zsh-syntax-highlighting
    zgen load zsh-users/zsh-autosuggestions
    zgen load zsh-users/zsh-completions
    zgen load mfaerevaag/wd

    # generate the init script from plugins above (remember to call zgen update and zgen reset)
    zgen save
fi

# ctrl+Space to accept autosuggestions
bindkey '^ ' autosuggest-accept

# fix colors
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#585858"


# --- functions and aliases ---
# images
function img {
    sxiv -r $@ >/dev/null 2>&1 &
}

# pdf
function pdf {
    if [[ $1 == *.pdf ]]; then
        evince $1 >/dev/null 2>&1 &
    elif [[ -d $1 ]]; then
        for f in $1/*.pdf; do
            evince $f >/dev/null 2>&1 &
        done
    fi
}

# weather
function wttr {
    if [ -n "$1" ]; then
        curl wttr.in/$1
    else
        curl wttr.in
    fi
}

# additional git aliases
alias gdn="git diff --name-only"
alias gdcs="git diff --compact-summary"
alias gstw="git stash show"
alias gstap="git stash push --patch"
alias gcop="git checkout --patch"
alias gcln="git clean --dry-run"
alias gclf="git clean --force"
alias lg="lazygit"

# additional kubectl aliases
alias kg="kubectl get"
alias kd="kubectl describe"

# dive
alias dive="docker run -ti --rm  -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive"

# git merge squash
function gms {
    trackingBranch=$1
    git merge --squash $trackingBranch || { echo 'merge-squashing failed' ; return 1; }
    git commit -m "squashed $trackingBranch"
}

# git cherry-spit
function gcsp {
    if [ "$#" -ne 2 ]; then
        echo "please give me 2 parameters (a commit hash and a branch name)"
        return 1;
    fi
    echo "cherry-spitting $1 onto $2"
    git checkout $2 || { echo 'cherry-spit failed' ; return 1; }
    git cherry-pick $1 || { return 1; }
    git checkout -
}

# Make watch expand aliases
alias watch='watch '

# terraform
alias tf="terraform"

# fastfetch
alias distroinfo="fastfetch"

# Mise
eval "$(/home/fwege/.local/bin/mise activate zsh)"

# --- Source fzf, completion and bindings if they exists ---
[[ -f ~/.fzf.zsh ]] && source ~/.fzf.zsh
[[ -f /usr/share/fzf/completion.zsh ]] && source /usr/share/fzf/completion.zsh
[[ -f /usr/share/fzf/key-bindings.zsh ]] && source /usr/share/fzf/key-bindings.zsh
[[ -f /usr/share/doc/fzf/examples/completion.zsh ]] && source /usr/share/doc/fzf/examples/completion.zsh
[[ -f /usr/share/doc/fzf/examples/key-bindings.zsh ]] && source /usr/share/doc/fzf/examples/key-bindings.zsh

# tabtab source for packages
# uninstall by removing these lines
[[ -f ~/.config/tabtab/__tabtab.zsh ]] && . ~/.config/tabtab/__tabtab.zsh || true

# Source google-cloud-cli (from aur package)
if [ -f '/etc/profile.d/google-cloud-cli.sh' ]; then source '/etc/profile.d/google-cloud-cli.sh'; fi

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/fwege/opt/google-cloud-sdk/path.zsh.inc' ]; then . '/home/fwege/opt/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/fwege/opt/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/fwege/opt/google-cloud-sdk/completion.zsh.inc'; fi
