#!/bin/bash

CURRENT_WORKSPACE=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' | cut -d"\"" -f2)
TARGET_WORKSPACE=$1
if [ $TARGET_WORKSPACE -ne $CURRENT_WORKSPACE ]; then
    i3-msg "rename workspace $CURRENT_WORKSPACE to temp; rename workspace $TARGET_WORKSPACE to $CURRENT_WORKSPACE; rename workspace temp to $TARGET_WORKSPACE;"
fi
